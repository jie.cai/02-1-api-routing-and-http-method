package com.twuc.webApp.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ApiControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_match_path_variable() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().isOk())
                .andExpect(content().string("The book for user 2"));
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(content().string("The book for user 23"));
    }

    @Test
    void should_first_match_const_path() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(status().isOk())
                .andExpect(content().string("const good"));
        mockMvc.perform(get("/api/segments/123"))
                .andExpect(status().isOk())
                .andExpect(content().string("id 123"));
    }

    @Test
    void should_match_single_chart_path() throws Exception {
        mockMvc.perform(get("/api/single/a"))
                .andExpect(status().isOk())
                .andExpect(content().string("hello"));
        mockMvc.perform(get("/api/single/aa"))
                .andExpect(status().isNotFound());
        mockMvc.perform(get("/api/single/"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_match_path_with_end_wildcards() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything"))
                .andExpect(content().string("hello"));
    }

    @Test
    void should_match_path_with_center_wildcards() throws Exception {
        mockMvc.perform(get("/api/wildcards/before/a/after"))
                .andExpect(content().string("hello"));
        mockMvc.perform(get("/api/wildcards/before/after"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_match_path_with_center_double_wildcards() throws Exception {
        mockMvc.perform(get("/api/wildcards/double/before/after"))
                .andExpect(content().string("hello"));
        mockMvc.perform(get("/api/wildcards/double/before/a/after"))
                .andExpect(content().string("hello"));
        mockMvc.perform(get("/api/wildcards/double/before/a/b/after"))
                .andExpect(content().string("hello"));
    }

    @Test
    void should_match_path_by_regexp() throws Exception {
        mockMvc.perform(get("/api/regexp/foo"))
                .andExpect(content().string("foo"));
        mockMvc.perform(get("/api/regexp/bar"))
                .andExpect(content().string("bar"));
        mockMvc.perform(get("/api/regexp/wow"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_get_by_params() throws Exception {
        mockMvc.perform(
                get("/api/student")
                    .param("gender", "female")
                    .param("class", "a1")
        )
                .andExpect(status().isOk())
                .andExpect(content().string("student is female, class a1"));
    }

    @Test
    void should_not_get_result_by_rejected_param() throws Exception {
        mockMvc.perform(
                get("/api/reject/some/param")
                        .param("wow", "HACK")
        )
                .andExpect(status().isBadRequest());
    }
}
