package com.twuc.webApp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {

    // # 2.1
/*
    @GetMapping("/{id}/books")
    public String getBooks(@PathVariable int id) {
        return "The book for user " + id;
    }
*/

    // # 2.2
    @GetMapping("/users/{id}/books")
    public String getBooksWithRename(@PathVariable("id") int targetId) {
        return "The book for user " + targetId;
    }

    // # 2.3
    @GetMapping("/segments/good")
    public String getConstGood() {
        return "const good";
    }
    @GetMapping("/segments/{id}")
    public String getGoodById(@PathVariable int id) {
        return "id " + id;
    }

    // # 2.4
    @GetMapping("/single/?")
    public String getBySingleMatch() {
        return "hello";
    }

    // # 2.5
    @GetMapping("/wildcards/*")
    public String wildcards() {
        return "hello";
    }

    // # 2.5
    @GetMapping("/wildcards/before/*/after")
    public String wildcardsCenter() {
        return "hello";
    }

    // # 2.6
    @GetMapping("/wildcards/double/before/**/after")
    public String wildcardsDoubleCenter() {
        return "hello";
    }

    // # 2.7
    @GetMapping("/regexp/{name:foo|bar}")
    public String regexp(@PathVariable("name") String pathName) {
        return pathName;
    }

    // # 2.8
    @GetMapping("/student")
    public String getByParams(
            @RequestParam String gender,
            @RequestParam("class") String klass
    ) {
        return "student is " + gender+", class " + klass;
    }

    @GetMapping(value = "/reject/some/param", params = {"!wow"})
    public String rejectSomeParam() {
        return "hello";
    }
}
